
const FIRST_NAME = "Cazacu";
const LAST_NAME = "Andrei";
const GRUPA = "1090";

function numberParser(value) {
    if(value <= Number.MIN_SAFE_INTEGER || value >= Number.MAX_SAFE_INTEGER)
        return NaN;

    return parseInt(value);
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}

